import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpclient: HttpClient) { }
  getData(url){
    return this.httpclient.get(url);
  }

  postData(url,obj){
    return this.httpclient.post(url,obj);
  }

  putData(url,data){
    return this.httpclient.put(url,data);
  }

  deleteData(url){
    return this.httpclient.delete(url);
  }
}
