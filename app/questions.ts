export class Questions {
  text: string;
  options: string[];
  answer: number;
}

export class Ques {
  id: number;
  qnText: string;
  A: String;
  B: string;
  C: string;
  D: string;
  ans: string;
}

export let questionData: Questions[] = [
  {text: 'What is the capital of India', options: ['New Delhi', 'London', 'Paris', 'Tokyo'], answer: 1},
{text: 'What is the capital of Italy', options: ['Berlin', 'London', 'Rome', 'Paris'], answer: 3},
{text: 'What is the capital of China', options: ['Shanghai', 'HongKong', 'Shenzen', 'Beijing'], answer: 4},
{text: 'What is the capital of Nepal', options: ['Tibet', 'Kathmandu', 'Colombo', 'Kabul'], answer: 2},
{text: 'What is the capital of Iraq', options: ['Baghdad', 'Dubai', 'Riyadh', 'Teheran'], answer: 1},
{text: 'What is the capital of Bangladesh', options: ['Teheran', 'Kabul', 'Colombo', 'Dhaka'], answer: 4},
{text: 'What is the capital of Sri Lanka', options: ['Islamabad', 'Colombo', 'Maldives', 'Dhaka'], answer: 2},
{text: 'What is the capital of Saudi Arabia', options: ['Baghdad', 'Dubai', 'Riyadh', 'Teheran'], answer: 1},
{text: 'What is the capital of France', options: ['London', 'New York', 'Paris', 'Rome'], answer: 3},
{text: 'What is the capital of Italy', options: ['Berlin', 'London', 'Paris', 'Rome'], answer: 4},
{text: 'What is the capital of Sweden', options: ['Helsinki', 'Stockholm', 'Copenhagen', 'Lisbon'], answer: 2},
{text: 'What is the currency of UK', options: ['Dollar', 'Mark', 'Yen', 'Pound'], answer: 4},
{text: 'What is the height of Mount Everest', options: ['9231 m', '8848 m', '8027 m', '8912 m'], answer: 2},
{text: 'What is the capital of Japan', options: ['Beijing', 'Osaka', 'Kyoto', 'Tokyo'], answer: 4},
{text: 'What is the capital of Egypt', options: ['Cairo', 'Teheran', 'Baghdad', 'Dubai'], answer: 1}];


export let mathsQuestion: Questions[] = [
  {
    text: 'What is 8 * 9',
    options: ['72', '76', '64', '81'],
    answer: 1
  },
  {
    text: 'What is 2*3+4*5',
    options: ['70', '50', '26', '60'],
    answer: 3
  }
];

export let chemistryQuestions: Questions[] =  [
  {
    text: 'What is the melting point of ice',
    options: ['0F', '0C', '100C', '100F'],
    answer: 2
  },
  {
    text: 'What is the atomic number of Oxygen',
    options: ['6', '7', '8', '9'],
    answer: 3
  },
  {
    text: 'What is the atomic number of Carbon',
    options: ['6', '7', '8', '9'],
    answer: 1
  },
  {
    text: 'Which of these is an inert element',
    options: ['Flourine', 'Suphur', 'Nitrogen', 'Argon'],
    answer: 4
  },
  {
    text: 'What is 0 Celsius in Fahrenheit',
    options: ['0', '32', '20', '48'],
    answer: 2
  }
];

export let computerQuestions: Questions[] = [
  {
    text: 'How many bytes are there in 1 kilobyte',
    options: ['16', '256', '1024', '4096'],
    answer: 3
  },
  {
    text: 'Who developed ReactJS',
    options: ['Facebook', 'Google', 'Microsoft', 'Apple'],
    answer: 1
  },
  {
    text: 'Angular is supported by ',
    options: ['Facebook', 'Google', 'Microsoft', 'Twitter'],
    answer: 2
  },
  {
    text: 'C# was developed by ',
    options: ['Amazon', 'Google', 'Microsoft', 'Twitter'],
    answer: 3
  },
  {
    text: 'Bootstrap was developed by ',
    options: ['Apple', 'Google', 'Facebook', 'Twitter'],
    answer: 4
  },
  {
    text: 'AWS is provided by ',
    options: ['Apple', 'Amazon', 'Microsoft', 'Google'],
    answer: 2
  },
  {
    text: 'Azure is provided by ',
    options: ['Microsoft', 'Amazon', 'IBM', 'Google'],
    answer: 1
  },
  {
    text: 'Angular is a framework that uses ',
    options: ['Java', 'Python', 'C#', 'Typescript'],
    answer: 4
  }
];
export let questions1 = [
  {
    text: 'What is the capital of India',
    options: ['New Delhi', 'London', 'Paris', 'Tokyo'],
    answer: 1
  },
  {
    text: 'What is the capital of Italy',
    options: ['Berlin', 'London', 'Rome', 'Paris'],
    answer: 3
  },
  {
    text: 'What is the capital of China',
    options: ['Shanghai', 'HongKong', 'Shenzen', 'Beijing'],
    answer: 4
  },
  {
    text: 'What is the capital of Nepal',
    options: ['Tibet', 'Kathmandu', 'Colombo', 'Kabul'],
    answer: 2
  },
  {
    text: 'What is the capital of Iraq',
    options: ['Baghdad', 'Dubai', 'Riyadh', 'Teheran'],
    answer: 1
  },
  {
    text: 'What is the capital of Bangladesh',
    options: ['Teheran', 'Kabul', 'Colomdo', 'Dhaka'],
    answer: 4
  },
  {
    text: 'What is the capital of Sri Lanka',
    options: ['Islamabad', 'Colombo', 'Maldives', 'Dhaka'],
    answer: 2
  },
  {
    text: 'What is the capital of Saudi Arabia',
    options: ['Baghdad', 'Dubai', 'Riyadh', 'Teheran'],
    answer: 1
  },
  {
    text: 'What is the capital of France',
    options: ['London', 'New York', 'Paris', 'Rome'],
    answer: 3
  },
  {
    text: 'What is the capital of Germany',
    options: ['Frankfurt', 'Budapest', 'Prague', 'Berlin'],
    answer: 4
  },
  {
    text: 'What is the capital of Sweden',
    options: ['Helsinki', 'Stockholm', 'Copenhagen', 'Lisbon'],
    answer: 2
  },
  {
    text: 'What is the currency of UK',
    options: ['Dollar', 'Mark', 'Yen', 'Pound'],
    answer: 4
  },
  {
    text: 'What is the height of Mount Everest',
    options: ['9231 m', '8848 m', '8027 m', '8912 m'],
    answer: 2
  },
  {
    text: 'What is the capital of Japan',
    options: ['Beijing', 'Osaka', 'Kyoto', 'Tokyo'],
    answer: 4
  },
  {
    text: 'What is the capital of Egypt',
    options: ['Cairo', 'Teheran', 'Baghdad', 'Dubai'],
    answer: 1
  }
];

export let newQuestions: Ques[] =
[{id: 1, qnText: 'What is the square of 9', A: '90', B: '72', C: '81', D: '99', ans: 'C'},
{id: 2, qnText: 'What is the 11*16', A: '160', B: '176', C: '204', D: '166', ans: 'B'},
{id: 3, qnText: 'Which of the following is not a power of 2', A: '0',    B: '1',    C: '2',    D: '8',    ans: 'A'},
{id: 4, qnText: 'log 1 is equal to', A: '1', B: '10', C: '-1', D: '0', ans: 'D'},
{id: 5, qnText: 'log(ab) is equal to', A: '(loga) + (logb)', B: 'b(loga)', C: 'a(logb)', D: '(loga)(logb)', ans: 'A'},
{id: 6, qnText: 'The square root is equal to', A: '1.0', B: '1.25', C: '1.414', D: '1.462', ans: 'B'},
{id: 7, qnText: 'The binary representation of 10 is', A: '0110', B: '1001', C: '1010', D: '1100', ans: 'C'},
{id: 8, qnText: '11111 in binary represents ', A: '27', B: '15', C: '41', D: '31', ans: 'D'},
{id: 9, qnText: 'The absolute value of -10.5 is equal to ', A: '-10.5', B: '10.5', C: '10', D: '11', ans: 'B'},
{id: 10, qnText: 'The roots of the equation of (x-2)(x+3) = 0 are', A: '2, -3', B: '2, 3', C: '-2, 3', D: '-2, -3', ans: 'A'}];
