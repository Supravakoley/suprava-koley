import { Questions, mathsQuestion } from './../questions';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-maths',
  templateUrl: './maths.component.html',
  styleUrls: ['./maths.component.css']
})
export class MathsComponent implements OnInit {
quizStore: Questions[] = mathsQuestion;
currentQues;
AnswerOpt;
score = 0;
answerArray = [
  {
    text: 'What is 8 * 9',
    options: ['72', '76', '64', '81'],
    answered: null
  },
  {
    text: 'What is 2*3+4*5',
    options: ['70', '50', '26', '60'],
    answered: null
  }
];
num = 0;
currentanswer = 'Not answered';
subject = 2;
marksshow = false;
questionshow = true;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.currentQues = this.quizStore[0];
this.AnswerOpt = this.currentQues.options;
console.log(this.quizStore);
  }

  prev() {
    this.num--;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  next() {
    this.num++;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  answerCheck(i) {
    this.answerArray[this.num].answered = i;
    console.log(this.answerArray);
    if (i === 0) {
      this.currentanswer = 'A';
    } else if (i === 1) {
      this.currentanswer = 'B';
    } else if (i === 2) {
      this.currentanswer = 'C';
    } else if (i === 3) {
      this.currentanswer = 'D';
    } else {
      this.currentanswer = 'Not answered';

    }
    console.log(this.currentanswer);

    if (this.quizStore[this.num].answer === this.answerArray[this.num].answered + 1) {
      this.score++;
      console.log(this.score);
      localStorage.setItem('score2', JSON.stringify(this.score));
    }
  }

  marks() {
      this.marksshow = true;
      this.questionshow = false;

  }

}
