import { Questions, computerQuestions } from './../questions';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-computer',
  templateUrl: './computer.component.html',
  styleUrls: ['./computer.component.css']
})
export class ComputerComponent implements OnInit {

  quizStore: Questions[] = computerQuestions;
currentQues;
AnswerOpt;
score = 0;
answerArray =  [
  {
    text: 'How many bytes are there in 1 kilobyte',
    options: ['16', '256', '1024', '4096'],
    answered: null
  },
  {
    text: 'Who developed ReactJS',
    options: ['Facebook', 'Google', 'Microsoft', 'Apple'],
    answered: null
  },
  {
    text: 'Angular is supported by ',
    options: ['Facebook', 'Google', 'Microsoft', 'Twitter'],
    answered: null
  },
  {
    text: 'C# was developed by ',
    options: ['Amazon', 'Google', 'Microsoft', 'Twitter'],
    answered: null
  },
  {
    text: 'Bootstrap was developed by ',
    options: ['Apple', 'Google', 'Facebook', 'Twitter'],
    answered: null
  },
  {
    text: 'AWS is provided by ',
    options: ['Apple', 'Amazon', 'Microsoft', 'Google'],
    answered: null
  },
  {
    text: 'Azure is provided by ',
    options: ['Microsoft', 'Amazon', 'IBM', 'Google'],
    answered: null
  },
  {
    text: 'Angular is a framework that uses ',
    options: ['Java', 'Python', 'C#', 'Typescript'],
    answered: null
  }
];
num = 0;
currentanswer = 'Not answered';
subject = 4;
marksshow = false;
questionshow = true;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.currentQues = this.quizStore[0];
this.AnswerOpt = this.currentQues.options;
console.log(this.quizStore);
  }

  prev() {
    this.num--;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  next() {
    this.num++;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  answerCheck(i) {
    this.answerArray[this.num].answered = i;
    console.log(this.answerArray);
    if (i === 0) {
      this.currentanswer = 'A';
    } else if (i === 1) {
      this.currentanswer = 'B';
    } else if (i === 2) {
      this.currentanswer = 'C';
    } else if (i === 3) {
      this.currentanswer = 'D';
    } else {
      this.currentanswer = 'Not answered';

    }
    console.log(this.currentanswer);

    if (this.quizStore[this.num].answer === this.answerArray[this.num].answered + 1) {
      this.score++;
      console.log(this.score);
      localStorage.setItem('score4', JSON.stringify(this.score));
    }
  }

  marks() {
      this.marksshow = true;
      this.questionshow = false;

  }

}
