import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInProComponent } from './sign-in-pro.component';

describe('SignInProComponent', () => {
  let component: SignInProComponent;
  let fixture: ComponentFixture<SignInProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
