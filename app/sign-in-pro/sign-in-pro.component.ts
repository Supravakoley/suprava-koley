import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-in-pro',
  templateUrl: './sign-in-pro.component.html',
  styleUrls: ['./sign-in-pro.component.css']
})
export class SignInProComponent implements OnInit {
email = null; password = null;
url = "http://localhost:2410/login";

  constructor(private dataService: DataService,private router:Router,private auth:AuthService) { }

  ngOnInit() {
  }

  signin(){
   let obj={
    email:this.email,
    password: this.password
    }
    this.dataService.postData(this.url,obj).subscribe(data=>{
      console.log(data);
      this.auth.login(data);
    this.router.navigate(['/products']);
    },
    error=>{
      console.log(error);
      alert("Invalid credentials !");
    })
  }


}
