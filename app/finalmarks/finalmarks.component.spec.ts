import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalmarksComponent } from './finalmarks.component';

describe('FinalmarksComponent', () => {
  let component: FinalmarksComponent;
  let fixture: ComponentFixture<FinalmarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalmarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalmarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
