import { Questions, questionData, mathsQuestion, chemistryQuestions, computerQuestions } from './../questions';
import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-finalmarks',
  templateUrl: './finalmarks.component.html',
  styleUrls: ['./finalmarks.component.css']
})
export class FinalmarksComponent implements OnInit {
@Input() store;
@Input() sub;
array1: Questions[] = questionData;
array2: Questions[] = mathsQuestion;
array3: Questions[] = chemistryQuestions;
array4: Questions[] = computerQuestions;
arrayToComp = [];
finalArray = [];
home = false;
  constructor() {
    // if(this.sub === 1) {
    //   this.arrayToShow: Questions[] = questionData;
    // }
  }

  ngOnInit() {
    this.compare();
  }

  compare() {
    if (this.sub === 1) {
      this.arrayToComp = this.array1;
    } else if (this.sub === 2) {
      this.arrayToComp = this.array2;
    } else if (this.sub === 3) {
      this.arrayToComp = this.array3;
    } else if (this.sub === 4) {
      this.arrayToComp = this.array4;
    }
    console.log(this.arrayToComp);
    for (let i = 0; i < this.arrayToComp.length; i++) {
      console.log(this.store[i]);
      console.log(this.arrayToComp[i]);
      if (this.store[i].answered === null) {
        this.finalArray.push('n');
      } else if (this.arrayToComp[i].answer === this.store[i].answered + 1) {
        this.finalArray.push('r');
      } else if ( this.arrayToComp[i].answer !== this.store[i].answered + 1) {
        this.finalArray.push('w');
      }
    }
    console.log(this.finalArray);

  }
  back() {
    this.home = true;
  }

}
