import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {
@Input() cellArray;
@Output() moveDeterminer = new EventEmitter<number>();
copyArray = [];
sorted = [];
num = 0;
game = 'Reset';
moveOfGame;
  constructor() { }

  ngOnInit() {
  }

  generateValue(i) {
    if (this.cellArray[i].value !== 1 || this.cellArray[i].value !== 0) {
      this.num++;
      if ((this.num % 2) !== 0) {
        this.moveOfGame = 0;
      this.cellArray[i].value += 2;
      } else {
        this.moveOfGame = 1;
      this.cellArray[i].value++;
      }
      this.copyArray = [...this.cellArray];
      this.sorted = this.copyArray.sort(this.gameFunc);
      if (this.sorted[0].value !== -1) {
        this.game = 'New Game';
      }
      console.log(this.sorted[0].value);
      this.moveDeterminer.emit(this.moveOfGame);
    }

  }

  gameFunc(c1, c2) {
    return c1.value - c2.value;
  }

  resetingG() {
    this.cellArray.map(c => c.value = -1);
    this.moveOfGame = 1;
    this.moveDeterminer.emit(this.moveOfGame);
  }

}
