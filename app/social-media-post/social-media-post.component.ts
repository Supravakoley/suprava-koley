import { postdb } from './../post';
import { Component, OnInit, Input } from '@angular/core';
import {Post} from '../post';

@Component({
  selector: 'app-social-media-post',
  templateUrl: './social-media-post.component.html',
  styleUrls: ['./social-media-post.component.css']
})
export class SocialMediaPostComponent implements OnInit {
  @Input() post: Post;
  constructor() {
  }

  ngOnInit() {
  }

  addLikes(x: number) {
    this.post.numOfLikes = this.post.numOfLikes + x;
  }

  addShares(x: number) {
    this.post.numOfShares = this.post.numOfShares + x;
  }
}
