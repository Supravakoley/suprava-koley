import { Questions, questionData, mathsQuestion, chemistryQuestions, computerQuestions } from './../questions';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-checking',
  templateUrl: './checking.component.html',
  styleUrls: ['./checking.component.css']
})
export class CheckingComponent implements OnInit {
@Input() store;
@Input() subject;
@Input() quiz1: Questions[] = questionData;
@Input() quiz2: Questions[] = mathsQuestion;
@Input() quiz3: Questions[] = chemistryQuestions;
@Input() quiz4: Questions[] = computerQuestions;
quizStore = [];
currentQues;
AnswerOpt;
questionshow = true;
marksshow = false;
score = 0;
total = 15;
optionChoosen;



num = 0;
currentanswer = 'Not answered';
  constructor() { }

  ngOnInit() {
    if (this.subject == 1) {
      this.quizStore = this.quiz1;
    } else if(this.subject == 2) {
      this.quizStore = this.quiz2;
    } else if(this.subject == 3) {
      this.quizStore = this.quiz3;
    } else if(this.subject == 4) {
      this.quizStore = this.quiz4;
    }
this.currentQues = this.store[0];
this.AnswerOpt = this.currentQues.options;
console.log(this.currentQues, this.subject);
if(this.store[0].anwered !== null) {
this.checkAnswer();
}
  }

  prev() {
    this.num--;
    this.currentQues = this.store[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';
    this.checkAnswer();
  }

  next() {
    this.num++;
    this.currentQues = this.store[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';
    this.checkAnswer();
  }
  checkAnswer() {
    let n = this.store[this.num].answered;
  if (this.quizStore[this.num].answer === n + 1) {
    this.optionChoosen = 'Correct Answer';
  } else {
    this.optionChoosen = 'Wrong Answer';
  }
  console.log(this.quizStore[this.num].answer, n+1);
}
  answerCheck(i) {
    this.store[this.num].answered = i;
    console.log(this.store);
    if (i === 0) {
      this.currentanswer = 'A';
    } else if (i === 1) {
      this.currentanswer = 'B';
    } else if (i === 2) {
      this.currentanswer = 'C';
    } else if (i === 3) {
      this.currentanswer = 'D';
    }

    console.log(this.currentanswer);
    // this.answerStore.emit(this.answerArray);
    if (this.quizStore[this.num].answer === this.store[this.num].answered + 1) {
      this.score++;
      console.log(this.score);
      localStorage.setItem('score1', JSON.stringify(this.score));
    }



  }
  marks() {
    this.marksshow = true;
    this.questionshow = false;
  }

}
