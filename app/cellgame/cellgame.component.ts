import { AnimalsPictures , animals} from './../pictures';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cellgame',
  templateUrl: './cellgame.component.html',
  styleUrls: ['./cellgame.component.css']
})
export class CellgameComponent implements OnInit {
@Input() cells;
cellArray: AnimalsPictures[] = [];
num = 0;
open1 = null;
open2 = null;
pre1 = null;
pre2 = null;
copyArray = [];
  constructor() { }

  ngOnInit() {
    this.func();
  }

  func() {
  this.cellArray = [...this.cells];
  }

  generateChar(i) {
    this.num++;
    if (this.num % 2 !== 0) {
      this.open1 = i;
      this.cellArray[this.open1].open = true;
    } else if (this.num % 2 === 0) {
      this.open2 = i;
    }
    console.log(this.open1);
    console.log(this.open2);
    if (this.open1 !== null && this.open2 !== null) {
      if (this.num > 2) {
        this.cellArray[this.pre1].url = 'X';
        this.cellArray[this.pre2].url = 'X';
      }

      if (this.cellArray[this.open1].url === this.cellArray[this.open2].url) {
        this.cellArray[this.open1].open = true;
        this.cellArray[this.open2].open = true;
        this.pre1 = this.open1;
        this.pre2 = this.open2;
        this.open1 = null;
        this.open2 = null;
        } else {
          this.cellArray[this.open1].url = 'X';
          this.cellArray[this.open2].url = 'X';
          this.cellArray[this.open1].open = true;
        this.cellArray[this.open2].open = true;
        console.log(this.open1);
        console.log(this.open2);
        this.pre1 = this.open1;
        this.pre2 = this.open2;
        this.open1 = null;
        this.open2 = null;
        }
    }
  }

  resetingG() {
   this.cellArray = [
    {url: '../assets/images/animals/croc.svg', open: false}, {url: '../assets/images/animals/elephant.svg', open: false},
    {url: '../assets/images/animals/giraffe.svg', open: false}, {url: '../assets/images/animals/gorilla.svg', open: false},
    {url: '../assets/images/animals/koala.svg', open: false}, {url: '../assets/images/animals/polar-bear.svg', open: false},
    {url: '../assets/images/animals/tiger.svg', open: false}, {url: '../assets/images/animals/whale.svg', open: false},

    {url: '../assets/images/animals/croc.svg', open: false}, {url: '../assets/images/animals/elephant.svg', open: false},
    {url: '../assets/images/animals/giraffe.svg', open: false}, {url: '../assets/images/animals/gorilla.svg', open: false},
    {url: '../assets/images/animals/koala.svg', open: false}, {url: '../assets/images/animals/polar-bear.svg', open: false},
    {url: '../assets/images/animals/tiger.svg', open: false}, {url: '../assets/images/animals/whale.svg', open: false}
   ];
    console.log(this.cellArray);
  }
}
