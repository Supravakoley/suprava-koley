import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellgameComponent } from './cellgame.component';

describe('CellgameComponent', () => {
  let component: CellgameComponent;
  let fixture: ComponentFixture<CellgameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CellgameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellgameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
