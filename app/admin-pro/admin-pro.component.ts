import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-pro',
  templateUrl: './admin-pro.component.html',
  styleUrls: ['./admin-pro.component.css']
})
export class AdminProComponent implements OnInit {

  url = "http://localhost:2410/products";
  type = null; name = null;index = null;

  produsctData = null;
  types=['Dining','Drawing','Bedroom','Study'];
  sel_furniture = null;code = null;
  sel_type =null;
  constructor(private dataService:DataService,private router:Router,private route:ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
    this.left_panel();
  this.data_fetching();
this.name = this.auth.userName;
  }

  left_panel(){
    this.sel_type={
      name:this.types,
      selected: this.type
    };
    console.log(this.sel_type);

  }
  data_fetching(){
    this.route.paramMap.subscribe(params=>{
      this.type = params.get('type');
      this.sel_type.selected = this.type;
      console.log(this.type);
      console.log(this.sel_type);
      this.showProducts();
    });
    this.route.paramMap.subscribe(params=>{
      this.code = params.get('code');
      this.showProducts();
    });


  }
  urlFunc(){
    // if(!this.type)
    // this.url = this.url;

  }

  showProducts(){
   this.dataService.getData(this.url).subscribe(data=>{
     this.produsctData = data;
     if(this.type)
     this.produsctData = this.produsctData.filter(p=> p.category === this.type);
     if(this.code){
      this.sel_furniture = this.produsctData.find(p => p.prodCode === this.code);
    }
    console.log(this.produsctData);

   })
  }

  selected_furni(index){
    this.index = index;
      this.sel_furniture = this.produsctData[index];
      let myparam = {};
      // myparam['type']=this.sel_furniture.category;
      this.router.navigate(['/products/'+this.sel_furniture.category+'/'+this.sel_furniture.prodCode]);
  }

  selected_type(){
    console.log(this.sel_type);
    this.router.navigate(['/products/'+this.sel_type.selected]);

  }

  cart_func(){
    let url = "http://localhost:2410/cart";
    console.log(this.sel_furniture);
    this.dataService.postData(url,this.sel_furniture).subscribe(data=>{
      console.log(data);
    });
  }
}
