import { Ques, newQuestions} from './../questions';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-holder',
  templateUrl: './holder.component.html',
  styleUrls: ['./holder.component.css']
})
export class HolderComponent implements OnInit {
questions: Ques[] = newQuestions;
page = true;
qusetionPage = false;
createQuestionPage = false;
quesSet = false;
questionStructure = null;
questionPaper;
QuestionPapersArray = [];
quesPaperName;
selQuestion = null;
  constructor() { }

  ngOnInit() {
    this.updateValues();
  }

  updateValues() {
    this.questionStructure = this.questions.map(q => ( {
      id: q.id,
      qnText: q.qnText,
        A: q.A,
        B: q.B,
        C: q.C,
        D: q.D,
      selected: false
    }));
  }

  questionFunc() {
    this.page = false;
    this.qusetionPage = true;
    this.createQuestionPage = true;
    this.quesSet = false;

  }

  homeFunc(){
    this.page = true;
    this.qusetionPage = false;
    this.createQuestionPage = false;
    this.quesSet = false;

  }
  createFunc() {
    this.page = false;
    this.qusetionPage = false;
    this.createQuestionPage = true;
    this.quesSet = false;

  }
  showQues(){
    this.page = false;
    this.qusetionPage = false;
    this.createQuestionPage = false;
    this.quesSet = true;
  }
  emitChange() {
    let data = this.questionStructure.filter(q => q.selected);
    this.questionPaper = {
      name : this.quesPaperName,
      questions : data
    };
    // console.log(this.questionStructure.filter(q => q.selected));
  }
  submitQues(){
    this.QuestionPapersArray.push(this.questionPaper);
    this.questionPaper = [];
    console.log(this.QuestionPapersArray);
    this.questionStructure.map(q => this.QuesSel(q));
    this.quesPaperName = '';
  }

  QuesSel(q) {
    if(q.selected){
      q.selected = false;
    }
  }

}
