import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart-pro',
  templateUrl: './cart-pro.component.html',
  styleUrls: ['./cart-pro.component.css']
})
export class CartProComponent implements OnInit {
  chair=null;table=null;items_arr = null;
  cartData = null;item=[];showData = [];
url ="http://localhost:2410/cartdata";
  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.dataService.getData(this.url).subscribe(data=>{
      console.log(data);
      this.cartData = data;
      this.cartData.map(c=>{
        this.showData.push(
          {
            title:c.title,
            category:c.category,
            img:c.img,
            ingredients:c.ingredients,
            prodCode:c.prodCode,
            num:1
          }
        )
      })
    this.item_func();

    })
  }

  item_func(){

    this.cartData.map(c=>{
        console.log(c.ingredients);
        for(let j = 0;j<c.ingredients.length;j++){
          let count = 0;
            if(this.item.length>0){

                for(let i =0;i<this.item.length;i++){
                  if( this.item[i].ingName === c.ingredients[j].ingName)
                        {
                          this.item[i].qty += c.ingredients[j].qty;
                          count++;
                        }
                  else
                  continue;
                }
                if(count==0)
                this.item.push(c.ingredients[j]);
            }
            else
              this.item.push(c.ingredients[j]);
      }

    });
    console.log(this.item);
    console.log(this.cartData);

  }

  increase(index){
     this.cartData.push(this.showData[index]);
     console.log(this.cartData);
     this.showData[index].num++;
      this.item = [];
     this.item_func();
  }

}
