import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-marking-sheet',
  templateUrl: './marking-sheet.component.html',
  styleUrls: ['./marking-sheet.component.css']
})
export class MarkingSheetComponent implements OnInit {
@Input() store;
@Input() subject;
// @Input() original;

arrayNum;
copyStorage;
array = [];
home = false;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.calculate();
  }

  calculate() {
    console.log(this.store);
    this.store.map(n1 => this.marking(n1));
    localStorage.setItem('store', this.store);
  }
marking(n1) {
  if (n1.answered === 0) {
    this.array.push('A');
  } else if (n1.answered === 1) {
    this.array.push('B');
  } else if (n1.answered === 2) {
    this.array.push('C');
  } else if (n1.answered === 3) {
    this.array.push('D');
  } else {
    this.array.push('');

  }
}
question() {
  // console.log("clicked");
  // let path = '/questions';
  // this.router.navigate([path]);
}
homePage() {
this.home = true;
}
//  finalPage() {
//    let path = '/home';
//    let qparam = this.store;
//    this.router.navigate([path], {queryParams: qparam});
//  }
}
