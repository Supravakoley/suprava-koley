import { Questions , questionData} from './../questions';
import { Component, OnInit, Input, Output,  EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

// import { } from 'protractor';
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
@Output() answerStore = new EventEmitter<any[]>();
quizStore: Questions[] = questionData;
currentQues;
AnswerOpt;
questionshow = true;
marksshow = false;
score = 0;
total = 15;
subject = 1;
// answerArray = [ {num: 1, answered: null}, {num: 2, answered: null}, {num: 3, answered: null},
// {num: 4, answered: null}, {num: 5, answered: null}, {num: 6, answered: null}, {num: 7, answered: null}, {num: 8, answered: null},
//  {num: 9, answered: null}, {num: 10, answered: null}, {num: 11, answered: null}, {num: 12, answered: null}, {num: 13, answered: null},
//  {num: 14, answered: null}, {num: 15, answered: null}
// ];
answerArray =  [
  {text: 'What is the capital of India', options: ['New Delhi', 'London', 'Paris', 'Tokyo'], answered: null},
{text: 'What is the capital of Italy', options: ['Berlin', 'London', 'Rome', 'Paris'], answered: null},
{text: 'What is the capital of China', options: ['Shanghai', 'HongKong', 'Shenzen', 'Beijing'], answered: null},
{text: 'What is the capital of Nepal', options: ['Tibet', 'Kathmandu', 'Colombo', 'Kabul'], answered: null},
{text: 'What is the capital of Iraq', options: ['Baghdad', 'Dubai', 'Riyadh', 'Teheran'], answered: null},
{text: 'What is the capital of Bangladesh', options: ['Teheran', 'Kabul', 'Colombo', 'Dhaka'], answered: null},
{text: 'What is the capital of Sri Lanka', options: ['Islamabad', 'Colombo', 'Maldives', 'Dhaka'], answered: null},
{text: 'What is the capital of Saudi Arabia', options: ['Baghdad', 'Dubai', 'Riyadh', 'Teheran'], answered: null},
{text: 'What is the capital of France', options: ['London', 'New York', 'Paris', 'Rome'], answered: null},
{text: 'What is the capital of Italy', options: ['Berlin', 'London', 'Paris', 'Rome'], answered: null},
{text: 'What is the capital of Sweden', options: ['Helsinki', 'Stockholm', 'Copenhagen', 'Lisbon'], answered: null},
{text: 'What is the currency of UK', options: ['Dollar', 'Mark', 'Yen', 'Pound'], answered: null},
{text: 'What is the height of Mount Everest', options: ['9231 m', '8848 m', '8027 m', '8912 m'], answered: null},
{text: 'What is the capital of Japan', options: ['Beijing', 'Osaka', 'Kyoto', 'Tokyo'], answered: null},
{text: 'What is the capital of Egypt', options: ['Cairo', 'Teheran', 'Baghdad', 'Dubai'], answered: null}];

num = 0;
currentanswer = 'Not answered';
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
this.currentQues = this.quizStore[0];
this.AnswerOpt = this.currentQues.options;
console.log(this.quizStore);

  }

  prev() {
    this.num--;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  next() {
    this.num++;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  answerCheck(i) {
    this.answerArray[this.num].answered = i;
    console.log(this.answerArray);
    if (i === 0) {
      this.currentanswer = 'A';
    } else if (i === 1) {
      this.currentanswer = 'B';
    } else if (i === 2) {
      this.currentanswer = 'C';
    } else if (i === 3) {
      this.currentanswer = 'D';
    } else {
      this.currentanswer = 'Not answered';

    }
    console.log(this.currentanswer);
    // this.answerStore.emit(this.answerArray);
    if (this.quizStore[this.num].answer === this.answerArray[this.num].answered + 1) {
      this.score++;
      console.log(this.score);
      localStorage.setItem('score1', JSON.stringify(this.score));
    }



  }
  marks() {
    this.marksshow = true;
    this.questionshow = false;
  }
}
