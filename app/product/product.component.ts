import { AuthService } from "./../auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from "./../data.service";
import { Component, OnInit } from "@angular/core";
import { formGroupNameProvider } from "@angular/forms/src/directives/reactive_directives/form_group_name";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.css"],
})
export class ProductComponent implements OnInit {
  url = "http://localhost:2410/products";
  type = null;
  name = null;
  index = null;

  produsctData = null;
  proPage = true;
  editPage = false;
  addPage = false;
  types = ["Dining", "Drawing", "Bedroom", "Study"];
  sel_furniture = null;
  code = null;
  sel_type = null;
  modification = null;
  editProd = null;
  prod_title = null;

  prodForm: FormGroup;
  description: FormArray;
  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.left_panel();
    this.data_fetching();
    this.name = this.auth.userName;
    this.formGroup_func();
  }

  left_panel() {
    this.sel_type = {
      name: this.types,
      selected: this.type,
    };
    console.log(this.sel_type);
  }
  data_fetching() {
    this.route.paramMap.subscribe((params) => {
      this.type = params.get("type");
      this.sel_type.selected = this.type;
      console.log(this.type);
      console.log(this.sel_type);
    });
    this.route.paramMap.subscribe((params) => {
      this.code = params.get("code");
    });
    this.route.paramMap.subscribe((params) => {
      this.modification = params.get("modification");

    });

    setTimeout(() => {
      this.urlFunc();
    }, 700);
  }
  urlFunc() {
    if (this.modification === 'edit')
    this.edit(this.code);
    else if(this.modification === 'add')
    this.add_item();
    else this.showProducts();
  }

  showProducts() {
    this.dataService.getData(this.url).subscribe((data) => {
      this.produsctData = data;
      if (this.type)
        this.produsctData = this.produsctData.filter(
          (p) => p.category === this.type
        );
      if (this.code) {
        this.sel_furniture = this.produsctData.find(
          (p) => p.prodCode === this.code
        );
      }
      console.log(this.produsctData);
    });
  }

  selected_furni(index) {
    this.index = index;
    this.sel_furniture = this.produsctData[index];
    let myparam = {};
    // myparam['type']=this.sel_furniture.category;
    this.router.navigate([
      "/products/" +
        this.sel_furniture.category +
        "/" +
        this.sel_furniture.prodCode,
    ]);
  }

  selected_type() {
    console.log(this.sel_type);
    this.router.navigate(["/products/" + this.sel_type.selected]);
  }

  cart_func() {
    let url = "http://localhost:2410/cart";
    console.log(this.sel_furniture);
    this.dataService.postData(url, this.sel_furniture).subscribe((data) => {
      console.log(data);
    });
  }
  control_func(type) {
    console.log(type);
    if(type === 'edit'){
    this.router.navigate([
      "/products/" +
        this.sel_furniture.category +
        "/" +
        this.sel_furniture.prodCode +
        "/" +
        type,

    ]);
  }
  else{
    this.router.navigate([
      "/product/" + type,

    ]);
  }
  }
  formGroup_func() {
    this.prodForm = this.formBuilder.group({
      podCode: new FormControl("", [Validators.required]),
      category: new FormControl("", [Validators.required]),
      img: new FormControl("", [Validators.required]),
      name: new FormControl("", [Validators.required]),

      description: this.formBuilder.array([this.createDesc()]),
    });
  }
  createDesc() {
    return this.formBuilder.group({
      desc: new FormControl("", [Validators.required]),
    });
  }
  add() {
    this.description = this.prodForm.get("description") as FormArray;
    this.description.push(this.createDesc());
  }
  desc_func(dataArr){
    console.log(dataArr,dataArr.length);
  for(let i = 0; i< dataArr.length;i++){
    console.log(dataArr[i]);
    let d = dataArr[i];
    this.prodForm.get("description").setValue(d);
    this.description = this.prodForm.get("description") as FormArray;

    // this.description = d;
    // console.log(this.description);
    // this.description.push(this.createDesc());

    // this.description.map(item => {
    //   formArray.push(this.createForms(item));
    // });
  }
  }
  edit(code) {
    console.log(code);
    let url = "http://localhost:2410/product/" + code;
    this.dataService.getData(url).subscribe((data) => {
      console.log(data);
      this.editProd = data[0];
      this.proPage = false;
      this.editPage = true;
      this.prodForm.get("podCode").setValue(this.editProd.prodCode);
      this.prodForm.get("category").setValue(this.editProd.category);
      this.prodForm.get("img").setValue(this.editProd.img);
      this.prodForm.get("name").setValue(this.editProd.title);
      console.log(this.editProd.desc)
      this.desc_func(this.editProd.desc);
      // this.prodForm.get("description").setValue(this.editProd.desc);
      // console.log("desc",  this.prodForm.get("description"));
      // this.add();
    });

  }
  add_item(){
    this.proPage = false;
    this.editPage = false;
    this.addPage = true;
  }
}
