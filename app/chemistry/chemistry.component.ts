import { Questions, chemistryQuestions } from './../questions';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-chemistry',
  templateUrl: './chemistry.component.html',
  styleUrls: ['./chemistry.component.css']
})
export class ChemistryComponent implements OnInit {

  quizStore: Questions[] = chemistryQuestions;
currentQues;
AnswerOpt;
score = 0;
answerArray =  [
  {
    text: 'What is the melting point of ice',
    options: ['0F', '0C', '100C', '100F'],
    answered: null
  },
  {
    text: 'What is the atomic number of Oxygen',
    options: ['6', '7', '8', '9'],
    answered: null
  },
  {
    text: 'What is the atomic number of Carbon',
    options: ['6', '7', '8', '9'],
    answered: null
  },
  {
    text: 'Which of these is an inert element',
    options: ['Flourine', 'Suphur', 'Nitrogen', 'Argon'],
    answered: null
  },
  {
    text: 'What is 0 Celsius in Fahrenheit',
    options: ['0', '32', '20', '48'],
    answered: null
  }
];
num = 0;
currentanswer = 'Not answered';
subject = 3;
marksshow = false;
questionshow = true;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.currentQues = this.quizStore[0];
this.AnswerOpt = this.currentQues.options;
console.log(this.quizStore);
  }

  prev() {
    this.num--;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  next() {
    this.num++;
    this.currentQues = this.quizStore[this.num];
    this.AnswerOpt = this.currentQues.options;
    this.currentanswer = 'Not answered';

  }

  answerCheck(i) {
    this.answerArray[this.num].answered = i;
    console.log(this.answerArray);
    if (i === 0) {
      this.currentanswer = 'A';
    } else if (i === 1) {
      this.currentanswer = 'B';
    } else if (i === 2) {
      this.currentanswer = 'C';
    } else if (i === 3) {
      this.currentanswer = 'D';
    } else {
      this.currentanswer = 'Not answered';

    }
    console.log(this.currentanswer);

    if (this.quizStore[this.num].answer === this.answerArray[this.num].answered + 1) {
      this.score++;
      console.log(this.score);
      localStorage.setItem('score3', JSON.stringify(this.score));
    }
  }

  marks() {
      this.marksshow = true;
      this.questionshow = false;

  }

}
