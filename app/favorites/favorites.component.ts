import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {
@Input() displayChoosenPic: string;
@Output() removingFavPic = new EventEmitter<string>();
pictureArray = this.displayChoosenPic;
ch = false;
  constructor() {
   }

  ngOnInit() {
  }

  check() {
    if (this.displayChoosenPic.length > 0) {
      console.log(this.displayChoosenPic);
      this.ch = true;
    }
  }

  remove() {
    this.removingFavPic.emit(this.displayChoosenPic);
  }

}
