import { ActivatedRoute, Router } from '@angular/router';
import { StoringService } from './../storing.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-bank',
  templateUrl: './question-bank.component.html',
  styleUrls: ['./question-bank.component.css']
})
export class QuestionBankComponent implements OnInit {
@Input() questionStore;
len = 0;
store = [];
homeCheck = false;
page = true;
  constructor(private storingservice: StoringService, private route: ActivatedRoute, private router: Router) {
    // tslint:disable-next-line: triple-equals
    if (this.questionStore !== undefined) {
      this.len = this.questionStore.length;
      }
  }

  ngOnInit() {
// if (this.questionStore !== undefined) {
//   this.store = this.questionStore;
// }
this.store = this.storingservice.questionStore;
console.log(this.store);
  }
  home1() {
    this.homeCheck = true;
    this.page = false;
  }
  edit(i) {
    this.page = false;
    let path = '/addquestion';
    let myparams = {};
    myparams['id'] = i;
    this.router.navigate([path], {queryParams: myparams});
  }
   remove(i) {
     this.store.splice(i, 1);
   }

}
