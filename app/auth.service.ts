import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
loginStatus = false;
userName = null;
  constructor() { }

  login(obj){
    this.loginStatus = true;
    this.userName = obj.name;
  }
  logout(){
    this.loginStatus = false;
    this.userName = null;
  }
}
