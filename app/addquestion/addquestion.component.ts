import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { StoringService } from './../storing.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addquestion',
  templateUrl: './addquestion.component.html',
  styleUrls: ['./addquestion.component.css']
})
export class AddquestionComponent implements OnInit {
qusetion;
opt1; opt2; opt3; opt4;
correctAnswer;
homeCheck = false;
page = true;
id = null;
editStore = [];
currentEdit;
questionStore = [];
editForm =false;
  constructor(private storingservice: StoringService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];

    });
    if (this.id !== undefined) {
      this.editFunc();
    }
    console.log(this.id);

  }
home1() {
  this.homeCheck = true;
  this.page = false;
  this.editForm = false;
}

addData() {
  let obj = {
    ques: this.qusetion,
    option1: this.opt1,
    option2: this.opt2,
    option3: this.opt3,
    option4: this.opt4,
    answer: this.correctAnswer
  };
  this.questionStore.push(obj);
  this.storingservice.add(obj);
  console.log(this.questionStore);
  this.qusetion = '';
  this.opt1 = '';
  this.opt2 = '';
  this.opt3 = '';
  this.opt4 = '';
  this.correctAnswer = '';
}

editFunc() {
  this.page = false;
  this.editForm = true;
  this.editStore = this.storingservice.questionStore;
  this.currentEdit = this.editStore[this.id];
  console.log(this.currentEdit);
  this.qusetion = this.currentEdit.ques;
  this.opt1 = this.currentEdit.option1;
  this.opt2 = this.currentEdit.option2;
  this.opt3 = this.currentEdit.option3;
  this.opt4 = this.currentEdit.option4;
  this.correctAnswer = this.currentEdit.answer;
}

editData() {
  let obj = {
    ques: this.qusetion,
    option1: this.opt1,
    option2: this.opt2,
    option3: this.opt3,
    option4: this.opt4,
    answer: this.correctAnswer
  };
  // this.questionStore[this.id] = obj;
  this.editStore[this.id] = obj;
  console.log(this.editStore);
  this.storingservice.questionStore = this.editStore;
  console.log(this.storingservice.questionStore);

}

}
