import { CartProComponent } from './cart-pro/cart-pro.component';
import { SignInProComponent } from './sign-in-pro/sign-in-pro.component';
import { ProductComponent } from './product/product.component';
import { DefaultComponent } from './default/default.component';
import { AddquestionComponent } from './addquestion/addquestion.component';
import { ComputerComponent } from './computer/computer.component';
import { ChemistryComponent } from './chemistry/chemistry.component';
import { MathsComponent } from './maths/maths.component';
import { HomeComponent } from './home/home.component';
import { QuestionComponent } from './question/question.component';
import { MarkingSheetComponent } from './marking-sheet/marking-sheet.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// const routes: Routes = [

//   // {
//   //   path: 'android',
//   // component: ByeComponent
//   // },
//   // {
//   //   path: 'java',
//   // component: JavaComponent
//   // }
// ];
// <button class="btn btn-secondady m-1" routerLink="/android" routerLinkActive="active">Android</button>

const routes: Routes = [
  // {
  //   path: '',
  //   component: HomeComponent
  // },
  // {
  //   path: 'home',
  //   component: HomeComponent
  // },
  // {
  //   path: 'marksheet',
  // component: MarkingSheetComponent
  // },

  // {
  //   path: 'questions',
  // component: QuestionComponent
  // },
  // {
  //   path: 'mathsquestions',
  //   component: MathsComponent
  // },
  // {
  //   path: 'chemquestions',
  //   component: ChemistryComponent
  // },
  // {
  //   path: 'compquestions',
  //   component: ComputerComponent
  // }
  // {
  //   path: 'addquestion',
  //   component: AddquestionComponent
  // }
  //-------------------------------//
  {
    path: '',
    component: DefaultComponent
  },
  {
    path: 'signIn',
    component: SignInProComponent
  },
  {
    path: 'product/:modification',
    component: ProductComponent
  },
  {
    path: 'products/:type/:code/:modification',
    component: ProductComponent
  },
  {
    path: 'products/:type/:code',
    component: ProductComponent
  },
  {
    path: 'products/:type',
    component: ProductComponent
  },
  {
    path: 'products',
    component: ProductComponent
  },
  {
    path: 'cart',
    component: CartProComponent
  }
  //-------------------------------//
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
