import { TestBed } from '@angular/core/testing';

import { StoringService } from './storing.service';

describe('StoringService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoringService = TestBed.get(StoringService);
    expect(service).toBeTruthy();
  });
});
