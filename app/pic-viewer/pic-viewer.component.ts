import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-pic-viewer',
  templateUrl: './pic-viewer.component.html',
  styleUrls: ['./pic-viewer.component.css']
})
export class PicViewerComponent implements OnInit {
@Input() currentDisplayPic: string;
@Output() favoritePicDisplay = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  favPic() {
    this.favoritePicDisplay.emit(this.currentDisplayPic);
  }

}
