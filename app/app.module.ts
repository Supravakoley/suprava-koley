import { AuthService } from './auth.service';
import { DataService } from './data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SocialMediaPostComponent } from './social-media-post/social-media-post.component';
import { PicViewerComponent } from './pic-viewer/pic-viewer.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { CellComponent } from './cell/cell.component';
import { CellgameComponent } from './cellgame/cellgame.component';
import { QuestionComponent } from './question/question.component';
import { MarkingSheetComponent } from './marking-sheet/marking-sheet.component';
import { HomeComponent } from './home/home.component';
import { MathsComponent } from './maths/maths.component';
import { ChemistryComponent } from './chemistry/chemistry.component';
import { ComputerComponent } from './computer/computer.component';
import { FinalmarksComponent } from './finalmarks/finalmarks.component';
import { CheckingComponent } from './checking/checking.component';
import { AddquestionComponent } from './addquestion/addquestion.component';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { HolderComponent } from './holder/holder.component';
import { CarsComponent } from './cars/cars.component';
import { DefaultComponent } from './default/default.component';
import { ProductComponent } from './product/product.component';
import { SignInProComponent } from './sign-in-pro/sign-in-pro.component';
import { CartProComponent } from './cart-pro/cart-pro.component';
import { AdminProComponent } from './admin-pro/admin-pro.component';

@NgModule({
  declarations: [AppComponent, CellgameComponent, QuestionComponent, MarkingSheetComponent, HomeComponent, MathsComponent,
    ChemistryComponent, ComputerComponent, FinalmarksComponent, CheckingComponent, AddquestionComponent, QuestionBankComponent, HolderComponent, CarsComponent, DefaultComponent, ProductComponent, SignInProComponent, CartProComponent, AdminProComponent],
  imports: [BrowserModule, AppRoutingModule, NgbModule, FormsModule, HttpClientModule, ReactiveFormsModule],
  providers: [DataService, AuthService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
