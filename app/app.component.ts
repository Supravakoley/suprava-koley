import { Router } from '@angular/router';
import { Questions, questionData } from './questions';
import { AnimalsPictures, animals } from './pictures';
import { Component, OnInit , Input} from '@angular/core';
import { Post, postdb } from './post';
import { TestBed } from '@angular/core/testing';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(private route:Router) {}

  ngOnInit() {
    this.route.navigate(['/products']);
  }


}
